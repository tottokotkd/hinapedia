const dest = '../dist', src = './src';

export function getConfig(isProduction) {
    return {
        browserSync: {
            server: {
                baseDir: ['../' +
                ''],
                routes: {
                    '/hinapedia': dest
                },
            },
            files: [dest],
            startPath: '/hinapedia'
        },
        markup: {
            src: src + "/markup/**",
            dest: dest,
        },
        browserify: {
            common: {
                cache: {}, packageCache: {}, fullPaths: false,
                extensions: ['.js', '.jsx'],
                debug: !isProduction,
            },
            debug: !isProduction,
            bundleConfigs: [{
                entries: src + '/app/entry.js',
                dest: dest,
                outputName: 'entry.js'
            }],
            extensions: ['.js', '.jsx'],
        },
        babel: {
            presets: ["es2017", "es2016", "es2015", "react", "stage-0"],
            plugins: isProduction ?
                [
                    "babel-plugin-transform-inline-environment-variables",
                    "transform-es2015-modules-commonjs",
                ] :
                [
                    "transform-es2015-modules-commonjs",
                    ["react-intl", {"messagesDir": "./extracted-messages/", "enforceDescriptions": true}],
                ]

        }
    };
}
