import * as React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {injectIntl} from 'react-intl'

import Component from '../../component/unit/AllUnitList'
import * as Selector from '../../selector'
import * as Action from '../../action/unit'

const mapStateToProps = state => {
    return {
        query: Selector.getUnitSearchQuery(state),
        mode: Selector.getUnitSearchMode(state),
        list : Selector.getAllUnitsList(state),
        page: Selector.getUnitListPage(state),
    }
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        reload: Action.loadAllUnits,
        setQuery: Action.setUnitListSearchQuery,
        setMode: Action.setSearchMode,
        setPage: Action.setUnitListPage,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Component));
