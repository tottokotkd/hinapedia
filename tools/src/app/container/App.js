import * as React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux';

import App from '../component/App'
import {selectMenuItem} from '../action/app'
import {searchIdol, setIdolListSearchQuery, setIdolListSortQuery} from '../action/idol'
import {searchUnit} from '../action/unit'

const mapStateToProps = state => { return {
    locale: state.user.lang.locale,
    messages: state.user.lang.messages
}};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        selectMenuItem: selectMenuItem,
        searchIdol: searchIdol,
        searchUnit: searchUnit,
        setIdolListSearchQuery: setIdolListSearchQuery,
        setIdolListSortQuery: setIdolListSortQuery,
    }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
