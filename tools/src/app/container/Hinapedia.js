/**
 * Created by tottokotkd on 31/01/2017.
 */

import * as React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {injectIntl} from 'react-intl'
import { bindActionCreators } from 'redux';
import flow from 'lodash/flow'

import Hinapedia from '../component/Hinapedia'
import * as Selector from '../selector'
import {selectMenuItem} from '../action/app'
import {loadAllIdols} from '../action/idol'
import {loadAllUnits} from '../action/unit'

const mapStateToProps = state => {
    return {
        selectedMenuItem: Selector.getSelectedMenuItem(state),
        loading: Selector.getIsIdolListLoading(state) || Selector.getIdolIndividualDataIsLoading(state) || Selector.getIsUnitListLoading(state) || Selector.getUnitIndividualDataIsLoading(state),
    }
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        loadAllIdols: loadAllIdols,
        loadAllUnits: loadAllUnits,
        selectMenuItem: selectMenuItem,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(flow([injectIntl, withRouter])(Hinapedia));
