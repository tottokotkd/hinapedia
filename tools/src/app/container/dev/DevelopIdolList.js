/**
 * Created by tottokotkd on 30/01/2017.
 */

import * as React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {injectIntl} from 'react-intl'

import DevelopIdolList from '../../component/dev/DevelopIdolList'
import * as Selector from '../../selector'
import * as Action from '../../action/idol'

const mapStateToProps = state => {
    return {
        query: Selector.getIdolSearchQuery(state),
        mode: Selector.getSearchMode(state),
        idols: Selector.getAllIdolsList(state),
        units: Selector.getAllUnitsList(state),
    }
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        reload: Action.loadAllIdols,
        setQuery: Action.setIdolListSearchQuery,
        setMode: Action.setSearchMode,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(DevelopIdolList));
