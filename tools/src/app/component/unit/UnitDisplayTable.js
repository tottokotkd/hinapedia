/**
 * Created by tottokotkd on 25/01/2017.
 */

import React from 'react'
const {Component, PropTypes} = React;
import {Link} from 'react-router'
import {intlShape} from 'react-intl';
import {Table, Button, Menu, Icon} from 'semantic-ui-react'
import orderBy from 'lodash/orderBy'
import clamp from 'lodash/clamp'
import flow from 'lodash/flow'
import slice from 'lodash/slice'
import range from 'lodash/range'
import toInt from 'lodash/toSafeInteger'
import defaultTo from 'lodash/defaultTo'

import {headerMessages} from '../../intl/messages'

class PagingMenu extends Component {

    render() {
        const {current, pages} = this.props;
        const calcMenuRange = (amount, {start, end}) => {
            if (amount == 0 || (start == 1 && end == pages)) {
                return {start, end}
            }

            if (start == 1) {
                return calcMenuRange(amount - 1, {start, end: end + 1});
            }

            if (end == pages) {
                return calcMenuRange(amount - 1, {start: start - 1, end});
            }

            if (amount % 2 == 0) {
                return calcMenuRange(amount - 1, {start: start - 1, end});
            } else {
                return calcMenuRange(amount - 1, {start, end: end + 1});
            }
        };
        const menuRange = calcMenuRange(4, {start: current, end: current});
        const items = range(menuRange.start, menuRange.end + 1);
        const onClick = flow((e, {name}) => name, toInt, this.props.onClick);
        return (
            <Menu pagination>
                {menuRange.start != 1 ? <Menu.Item as='a' icon name={String(1)} onClick={onClick} content={<Icon name='angle double left' />} /> : null}
                {current != 1 ? <Menu.Item as='a' icon name={String(current - 1)} onClick={onClick} content={<Icon name='angle left' />} /> : null}
                {items.map(item => <Menu.Item key={item} as='a' name={String(item)} active={item == current} onClick={onClick}/>)}
                {current != pages ? <Menu.Item as='a' icon name={String(current + 1)} onClick={onClick} content={<Icon name='angle right' />} /> : null}
                {menuRange.end != pages ? <Menu.Item as='a' icon name={String(pages)} onClick={onClick} content={<Icon name='angle double right' />} /> : null}
            </Menu>
        );
    }

}

PagingMenu.propTypes = {
    current: PropTypes.number,
    pages: PropTypes.number.isRequired,
    onClick: PropTypes.func,
};

PagingMenu.defaultProps = {
    current: 1,
    onClick: pageIndex => {},
};

const sortKey = {
    Name: Symbol('UnitDisplayTable sort key: name'),
    NumberOfMembers: Symbol('UnitDisplayTable sort key: number of members'),
    NumberOfCuteMembers: Symbol('UnitDisplayTable sort key: number of cute members'),
    NumberOfCoolMembers: Symbol('UnitDisplayTable sort key: number of cool members'),
    NumberOfPassionMembers: Symbol('UnitDisplayTable sort key: number of passion members'),
    NumberOfUnvoice: Symbol('UnitDisplayTable sort key: number of unvoice members'),
};

const sortOrder = {
    Asc: Symbol('UnitDisplayTable sort order: asc'),
    Desc: Symbol('UnitDisplayTable sort order: desc'),
};

class UnitDisplayTable extends Component {

    state = {
        page: 1,
        sortKey: null,
        sortOrder: null,
    };

    constructor(props) {
        super(props);

        this.sortOrderMap = {};
        this.sortOrderMap[sortOrder.Asc] = 'ascending';
        this.sortOrderMap[sortOrder.Desc] = 'descending';
    };

    pageControlled = () => this.props.page !== null;
    getPage = () => this.pageControlled() ? this.props.page : this.state.page;
    setPage = next => this.pageControlled() ? this.props.onPageChange({current: this.getPage(), next}) : this.setState({page: next});

    setSortKey = key => {
        const currentKey = this.state.sortKey;
        const currentOrder = this.state.sortOrder;

        const defaultOrder = key => {
            switch (key) {
                case sortKey.Name:
                case sortKey.NumberOfUnvoice:
                    return sortOrder.Asc;

                case sortKey.NumberOfMembers:
                case sortKey.NumberOfCuteMembers:
                case sortKey.NumberOfCoolMembers:
                case sortKey.NumberOfPassionMembers:
                    return sortOrder.Desc;
            }
        };

        if (currentKey != key) {
            this.setState({
                sortKey: key,
                sortOrder: defaultOrder(key),
            });
        } else if (currentOrder != defaultOrder(key)) {
            this.setState({
                sortKey: null,
                sortOrder: null,
            });
        } else if (currentOrder == sortOrder.Asc) {
            this.setState({
                sortOrder: sortOrder.Desc,
            });
        } else {
            this.setState({
                sortOrder: sortOrder.Asc,
            });
        }
    };

    addParams = units => units.map(unit => {
        const total = unit.idols.length;
        const memberCounts = {
            total,
            cu: unit.idols.filter(idol => idol.idolType === "Cu").length,
            co: unit.idols.filter(idol => idol.idolType === "Co").length,
            pa: unit.idols.filter(idol => idol.idolType === "Pa").length,
            unvoice: total == unit.actorCount ? null : total - unit.actorCount,
        };
        const tagURL = `http://www.pixiv.net/tags.php?tag=${unit.unitName}`;
        const dicURL = `http://dic.pixiv.net/a/${unit.unitName}`;
        return {...unit, memberCounts, tagURL, dicURL}
    });

    numberOfPages = () => this.props.itemPerPage > 0 ? Math.ceil(this.props.units.length / this.props.itemPerPage) : 1;

    sortUnits = units => {
        const order = this.state.sortOrder == sortOrder.Asc ? 'asc' : 'desc';
        switch (this.state.sortKey) {
            case sortKey.Name:
                return orderBy(units, unit => unit.unitName, order);

            case sortKey.NumberOfMembers:
                return orderBy(units, unit => unit.memberCounts.total, order);

            case sortKey.NumberOfCuteMembers:
                return orderBy(units, unit => unit.memberCounts.cu, order);

            case sortKey.NumberOfCoolMembers:
                return orderBy(units, unit => unit.memberCounts.co, order);

            case sortKey.NumberOfPassionMembers:
                return orderBy(units, unit => unit.memberCounts.pa, order);

            case sortKey.NumberOfUnvoice:
                return orderBy(units, unit => unit.memberCounts.unvoice, order);

            default:
                return units;
        }
    };

    addIndex = units => units.map((unit, index) => {return {...unit, index}});

    sliceUnits = units => {
        const itemPerPage = this.props.itemPerPage;
        if (itemPerPage === null) {
            return units;
        }

        const page = clamp(this.getPage(), 0, this.numberOfPages());
        return slice(units, (page - 1) * itemPerPage, page * itemPerPage);
    };

    render() {
        const {toCreateLinkToIdol, units} = this.props;
        const sortedUnits = flow(this.addParams, this.sortUnits, this.addIndex, this.sliceUnits)(units);
        const pages = this.numberOfPages();

        const headerCell = (key, content, sort, width) => {
            const {sortKey, sortOrder} = this.state;
            const sorted = sortKey != sort ? null : this.sortOrderMap[sortOrder];
            return (
                <Table.HeaderCell
                    key={key}
                    sorted={sorted}
                    width={width}
                    onClick={() => {
                        this.setPage(1);
                        this.setSortKey(sort);
                    }}
                    content={content}
                />
            );

        };

        return (

            <Table celled sortable unstackable definition>
                <Table.Header>
                    <Table.Row textAlign='center'>
                        <Table.HeaderCell />
                        {headerCell('Column1', 'Name', sortKey.Name, 4)}
                        {headerCell('Column2', 'Total', sortKey.NumberOfMembers)}
                        {headerCell('Column3', 'Cu', sortKey.NumberOfCuteMembers)}
                        {headerCell('Column4', 'Co', sortKey.NumberOfCoolMembers)}
                        {headerCell('Column5', 'Pa', sortKey.NumberOfPassionMembers)}
                        {headerCell('Column6', 'unvoice', sortKey.NumberOfUnvoice)}
                        <Table.HeaderCell width={10} content="Idols"/>
                        <Table.HeaderCell content="Pixiv"/>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {sortedUnits.map(unit => {
                        const {memberCounts, tagURL, dicURL, index} = unit;
                        const sortedIdols = orderBy(unit.idols, 'yomi');
                        return (
                            <Table.Row key={unit.unitID}>
                                <Table.Cell collapsing textAlign='center' content={index + 1}/>
                                <Table.Cell selectable collapsing content={<Link to={`/unit/${unit.unitName.replace('/', '_')}`}>{unit.unitName}</Link>}/>
                                <Table.Cell collapsing textAlign='center' content={memberCounts.total}/>
                                <Table.Cell collapsing textAlign='center' content={memberCounts.cu}/>
                                <Table.Cell collapsing textAlign='center' content={memberCounts.co}/>
                                <Table.Cell collapsing textAlign='center' content={memberCounts.pa}/>
                                <Table.Cell collapsing textAlign='center' content={defaultTo(memberCounts.unvoice, '-')}/>
                                <Table.Cell>
                                    {sortedIdols.map(idol =>
                                        <Button
                                            key={idol.characterName}
                                            disabled={!toCreateLinkToIdol(idol)}
                                            compact size='mini'
                                            content={<Link to={`/idol/${idol.characterName}`}>{idol.characterName}</Link>}
                                        />)}
                                </Table.Cell>
                                <Table.Cell collapsing>
                                    <a href={tagURL}>tag</a> / <a href={dicURL}>dic</a>
                                </Table.Cell>
                            </Table.Row>
                        );
                    })}
                </Table.Body>
                {pages != 1 ?
                    <Table.Footer fullWidth>
                        <Table.Row textAlign="right">
                            <Table.HeaderCell colSpan={9}>
                                <PagingMenu pages={pages} current={this.getPage()} onClick={this.setPage}/>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                    : null}

            </Table>
        )
    }
}

UnitDisplayTable.propTypes = {
    units: PropTypes.arrayOf(PropTypes.shape({
        unitID: PropTypes.number.isRequired,
        unitName: PropTypes.string.isRequired,
        idols: PropTypes.arrayOf(PropTypes.shape({
            characterID: PropTypes.number.isRequired,
            characterName: PropTypes.string.isRequired,
            yomi: PropTypes.string.isRequired,
            idolType: PropTypes.string.isRequired,
        })).isRequired,
    })).isRequired,
    itemPerPage: PropTypes.number,

    page: PropTypes.number,
    onPageChange: PropTypes.func,
    toCreateLinkToIdol: PropTypes.func,
};

UnitDisplayTable.defaultProps = {
    itemPerPage: 30,
    page: null,
    onPageChange: ({current, next}) => {},
    toCreateLinkToIdol: ({characterID, characterName, yomi, idolType}) => true,
};

export default UnitDisplayTable;
