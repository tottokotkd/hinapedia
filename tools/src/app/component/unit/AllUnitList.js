/**
 * Created by tottokotkd on 09/02/2017.
 */

import React from 'react'
const {Component, PropTypes} = React;
import {intlShape} from 'react-intl';
import {Input, Button, Grid, Header, Message} from 'semantic-ui-react'
import defaultTo from 'lodash/defaultTo'
import filter from 'lodash/filter'

import {UNIT_SEARCH_MODE} from '../../constant/Mode'
import {headerMessages} from '../../intl/messages'
import UnitDisplayTable from './UnitDisplayTable'

class AllUnitList extends Component {

    render() {
        const {list, setMode, mode, query, page, setQuery, setPage} = this.props;
        const {formatMessage} = this.props.intl;

        const shawOnlyMatched = !!query;
        const onChange = (event, {value}) => setQuery(value);

        const units = query ? filter(list, 'isMatch') : list ;

        return (
            <div>
                <Grid container verticalAlign='middle'>
                    <Grid.Row>
                        <Grid.Column>
                            <Message info icon='bomb'
                                     header='データ収集にご協力ください'
                                     content={<div>ユニットデータは現在整備中です。追加・修正リクエストはこちら (<a href="https://twitter.com/tottokotkd">Twitter</a> / <a href="https://gitlab.com/tottokotkd/hinapedia/issues/9">GitLab</a>)</div>}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Message info icon='circle info'
                                     header='現在の「ユニット」データに劇中劇のデータは含まれません'
                                     content="モバマス公演由来の用語は取り扱いを検討中のため、以下のリストに含まれません。"
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Header as='h3' content='ユニット一覧' subheader={`収録件数: ${list.length}件`}/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width="12">
                            <Input placeholder="query" value={defaultTo(query, '')} onChange={onChange} fluid />
                        </Grid.Column>
                        <Grid.Column width="4" >
                            <Button.Group size="mini" fluid>
                                <Button positive={mode == UNIT_SEARCH_MODE.AND} onClick={() => setMode(UNIT_SEARCH_MODE.AND)}>AND</Button>
                                <Button positive={mode == UNIT_SEARCH_MODE.OR} onClick={() => setMode(UNIT_SEARCH_MODE.OR)}>OR</Button>
                            </Button.Group>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <UnitDisplayTable units={units} itemPerPage={100} page={page} onPageChange={({next}) => setPage(next)}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

AllUnitList.propTypes = {
    query: PropTypes.string.isRequired,
    mode: PropTypes.symbol.isRequired,
    page: PropTypes.number.isRequired,
    list: UnitDisplayTable.propTypes.units,
    setQuery: PropTypes.func.isRequired,
    setMode: PropTypes.func.isRequired,
    setPage: PropTypes.func.isRequired,
    reload: PropTypes.func.isRequired,


    // react-intl
    intl: intlShape
};

export default AllUnitList;
