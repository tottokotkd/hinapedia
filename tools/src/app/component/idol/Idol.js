/**
 * Created by tottokotkd on 29/01/2017.
 */

import React, {Component, PropTypes} from 'react'
import {intlShape} from 'react-intl';
import {Link} from 'react-router'
import {Header, Table, Grid, List, Message} from 'semantic-ui-react'

import Unit from '../unit/UnitDisplayTable'
import {IDOL_SEARCH_STATUS} from '../../constant/Mode'

class Idol extends Component {

    render() {
        const {status} = this.props;

        switch (status) {
            case IDOL_SEARCH_STATUS.NOT_FOUND: {
                const {name} = this.props.params;
                return <div>{name} not found</div>;
            }

            case IDOL_SEARCH_STATUS.FOUND: {
                const {characterID, characterName, yomi, idolType, age, birthDay, birthMonth, actorName, units} = this.props.idol;
                const header = `${characterName} (${yomi.split(',').join(' - ')})`;
                const googleURL = `https://www.google.com/search?q=シンデレラガールズ+${characterName}`;
                const tagURL = `http://www.pixiv.net/tags.php?tag=${characterName}`;
                const dicURL = `http://dic.pixiv.net/a/${characterName}`;
                return (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <Message info icon='bomb'
                                         header='データ収集にご協力ください'
                                         content={<div>所属ユニットデータは現在整備中です。追加・修正リクエストはこちら (<a href="https://twitter.com/tottokotkd">Twitter</a> / <a href="https://gitlab.com/tottokotkd/hinapedia/issues/9">GitLab</a>)</div>}
                                />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Message info icon='circle info'
                                         header='現在の「ユニット」データに劇中劇のデータは含まれません'
                                         content="モバマス公演由来の用語は取り扱いを検討中のため、以下のリストに含まれません。"
                                />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column width={10}>
                                <Header as="h3" content={header}/>
                                <Header as='h4' content='Google'/>
                                <List link>
                                    <List.Item as='a' icon="linkify" href={googleURL} content="search"/>
                                </List>
                                <Header as='h4' content='Pixiv'/>
                                <List link>
                                    <List.Item as='a' icon="linkify" href={tagURL} content="tag"/>
                                    <List.Item as='a' icon="linkify" href={dicURL} content="dic"/>
                                </List>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <Header as='h4' attached='top' content='キャラクターデータ'/>
                                <Table definition attached='bottom'>
                                    <Table.Body>
                                        {
                                            [
                                                {title: '属性', value: idolType},
                                                {title: '年齢', value: age},
                                                {title: '誕生日', value: `${birthMonth}/${birthDay}`},
                                                {title: 'CV', value: actorName},
                                                {title: '管理ID', value: characterID},
                                            ].map(param =>
                                                <Table.Row key={param.title}>
                                                    <Table.Cell collapsing textAlign="center">{param.title}</Table.Cell>
                                                    <Table.Cell textAlign="left">{param.value}</Table.Cell>
                                                </Table.Row>
                                            )
                                        }
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Header as='h4'  content='所属ユニット'/>
                                <Unit units={units} toCreateLinkToIdol={target => target.characterID != characterID} itemPerPage={null}/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                );
            }

            default:
                return null;
        }
    }
}

Idol.propTypes = {
    status: PropTypes.symbol.isRequired,
    idol: PropTypes.shape({
        characterID: PropTypes.number.isRequired,
        characterName: PropTypes.string.isRequired,
        yomi: PropTypes.string.isRequired,
        idolType: PropTypes.string.isRequired,
        age: PropTypes.number,
        birthDay: PropTypes.number.isRequired,
        birthMonth: PropTypes.number.isRequired,
        actorName: PropTypes.string,
        units: PropTypes.arrayOf(PropTypes.shape({
            unitID: PropTypes.number.isRequired,
            unitName: PropTypes.string.isRequired,
            idols: PropTypes.arrayOf(PropTypes.shape({
                characterID: PropTypes.number.isRequired,
                characterName: PropTypes.string.isRequired,
                yomi: PropTypes.string.isRequired,
                idolType: PropTypes.string.isRequired,
                age: PropTypes.number,
                birthDay: PropTypes.number.isRequired,
                birthMonth: PropTypes.number.isRequired,
                actorName: PropTypes.string,
            })).isRequired,
        })).isRequired,
    }),

    // react-router
    params: PropTypes.shape({
        name: PropTypes.string.isRequired,
    }).isRequired,

    // react-intl
    intl: intlShape
};

export default Idol;
