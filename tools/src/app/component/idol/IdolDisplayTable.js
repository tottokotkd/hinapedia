/**
 * Created by tottokotkd on 09/02/2017.
 */

import React from 'react'
const {Component, PropTypes} = React;
import {Link} from 'react-router'
import {intlShape} from 'react-intl';
import {Table, Button} from 'semantic-ui-react'
import orderBy from 'lodash/orderBy'
import flow from 'lodash/flow'
import round from 'lodash/round'
import Clipboard from 'clipboard'

import reduce from 'lodash/reduce'

import {IDOL_SEARCH_MODE} from '../../constant/Mode'
import {headerMessages} from '../../intl/messages'

const sortKey = {
    Type: Symbol('IdolDisplayTable sort key: type'),
    Name: Symbol('IdolDisplayTable sort key: name'),
    Height: Symbol('IdolDisplayTable sort key: height'),
    Age: Symbol('IdolDisplayTable sort key: age'),
    NumberOfUnits: Symbol('IdolDisplayTable sort key: number of units'),
    NumberOfColleagues: Symbol('IdolDisplayTable sort key: number of colleagues'),
    NumberOfVoiceColleagues: Symbol('IdolDisplayTable sort key: number of voice colleagues'),
    RateOfVoiceColleagues: Symbol('IdolDisplayTable sort key: rate of voice colleagues'),
};

const sortOrder = {
    Asc: Symbol('IdolDisplayTable sort order: asc'),
    Desc: Symbol('IdolDisplayTable sort order: desc'),
};

class IdolDisplayTable extends Component {

    state = {
        page: 1,
        sortKey: null,
        sortOrder: null,
    };

    constructor(props) {
        super(props);

        this.sortOrderMap = {};
        this.sortOrderMap[sortOrder.Asc] = 'ascending';
        this.sortOrderMap[sortOrder.Desc] = 'descending';
    };

    componentDidMount() {
        const clip = new Clipboard('#copyButton');

        clip.on('error', e => {
            console.error('clipboard copy failed.');
        });
    }

    setSortKey = key => {
        const currentKey = this.state.sortKey;
        const currentOrder = this.state.sortOrder;

        const defaultOrder = key => {
            switch (key) {
                case sortKey.Name:
                case sortKey.Type:
                    return sortOrder.Asc;

                case sortKey.Age:
                case sortKey.Height:
                case sortKey.NumberOfUnits:
                case sortKey.NumberOfColleagues:
                case sortKey.NumberOfVoiceColleagues:
                case sortKey.RateOfVoiceColleagues:
                    return sortOrder.Desc;

                default:
                    return sortOrder.Desc;
            }
        };

        if (currentKey != key) {
            this.setState({
                sortKey: key,
                sortOrder: defaultOrder(key),
            });
        } else if (currentOrder != defaultOrder(key)) {
            this.setState({
                sortKey: null,
                sortOrder: null,
            });
        } else if (currentOrder == sortOrder.Asc) {
            this.setState({
                sortOrder: sortOrder.Desc,
            });
        } else {
            this.setState({
                sortOrder: sortOrder.Asc,
            });
        }
    };

    addParams = idols => idols.map(idol => {
        const tagURL = `http://www.pixiv.net/tags.php?tag=${idol.characterName}`;
        const dicURL = `http://dic.pixiv.net/a/${idol.characterName}`;
        return {...idol, tagURL, dicURL}
    });

    sortIdols = idols => {
        const order = this.state.sortOrder == sortOrder.Asc ? 'asc' : 'desc';
        switch (this.state.sortKey) {
            case sortKey.Type:
                return orderBy(idols, idol => idol.idolType, order);

            case sortKey.Name:
                return orderBy(idols, idol => idol.yomi, order);

            case sortKey.Age:
                return orderBy(idols, idol => idol.age, order);

            case sortKey.Height:
                return orderBy(idols, idol => idol.height, order);

            case sortKey.NumberOfUnits:
                return orderBy(idols, idol => idol.units.length, order);

            case sortKey.NumberOfColleagues:
                return orderBy(idols, idol => idol.colleagueCount, order);

            case sortKey.NumberOfVoiceColleagues:
                return orderBy(idols, idol => idol.cvColleagueCount, order);

            case sortKey.RateOfVoiceColleagues:
                return orderBy(idols, idol => idol.cvColleagueRate, order);

            default:
                return idols;
        }
    };

    addIndex = units => units.map((unit, index) => {return {...unit, index}});

    render() {
        const {idols, showAverage} = this.props;
        const sortedIdols = flow(this.addParams, this.sortIdols, this.addIndex)(idols);

        const headerCell = (key, content, sort, width) => {
            const {sortKey, sortOrder} = this.state;
            const sorted = sortKey != sort ? null : this.sortOrderMap[sortOrder];
            return (
                <Table.HeaderCell
                    key={key}
                    sorted={sorted}
                    width={width}
                    onClick={() => this.setSortKey(sort)}
                    content={content}
                />
            );

        };

        const average = !showAverage ? null : (() => {
                const average = flow(
                    arr => reduce(
                        arr,
                        (total, value) => value == null ? total : {count: total.count + 1, value: total.value + value},
                        {count: 0, value: 0},
                    ),
                    ({count, value}) => count == 0 ? null : value / count,
                );
                const calc = flow(average, num => num == null ? '-' : round(num, 2));
                const age = calc(sortedIdols.map(i => i.age));
                const height = calc(sortedIdols.map(i => i.height));
                const unitCount = calc(sortedIdols.map(i => i.units.length));
                const colleagueCount = calc(sortedIdols.map(i => i.colleagueCount));
                const cvColleagueCount = calc(sortedIdols.map(i => i.cvColleagueCount));
                const cvColleagueRate = calc(sortedIdols.map(i => i.cvColleagueRate));
                return {age, height, unitCount, colleagueCount, cvColleagueCount, cvColleagueRate}
            })();

        const copyText = (() => {
            const header = "Index,Type,Name,Age,Height,Units,Colleagues,(Voiced count),(Voiced rate)\n";
            // Type	Name	Age	Height	Units	Colleagues	(Voiced: count)	(Voiced: rate)
            const idols = sortedIdols
                .map((idol,index) => `${index + 1},${idol.idolType},${idol.characterName},${idol.age},${idol.height},${idol.unitCount},${idol.colleagueCount},${idol.cvColleagueCount},${idol.cvColleagueRate}`)
                .join("\n");
            return showAverage
                ? header + `average,-,-,${average.age},${average.height},${average.unitCount},${average.colleagueCount},${average.cvColleagueCount},${average.cvColleagueRate}\n` + idols
                : header + idols;
        })();

        return (
            <Table celled sortable unstackable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell id="copyButton" data-clipboard-text={copyText} />
                        {headerCell('Column1', 'Type', sortKey.Type)}
                        {headerCell('Column2', 'Name', sortKey.Name)}
                        {headerCell('Column3', 'Age', sortKey.Age)}
                        {headerCell('Column4', 'Height', sortKey.Height)}
                        {headerCell('Column5', 'Units', sortKey.NumberOfUnits)}
                        {headerCell('Column6', 'Colleagues', sortKey.NumberOfColleagues)}
                        {headerCell('Column7', '(Voiced count)', sortKey.NumberOfVoiceColleagues)}
                        {headerCell('Column8', '(Voiced rate)', sortKey.RateOfVoiceColleagues)}
                        <Table.HeaderCell>Pixiv</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {showAverage ? (() => {
                        const  {age, height, unitCount, colleagueCount, cvColleagueCount, cvColleagueRate} = average;
                        return (
                            <Table.Row warning>
                                <Table.Cell collapsing textAlign='center' content='-'/>
                                <Table.Cell collapsing textAlign='center' content='-'/>
                                <Table.Cell selectable content={<Link to={null}>average</Link>}/>
                                <Table.Cell collapsing textAlign='center' content={age}/>
                                <Table.Cell collapsing textAlign='center' content={height}/>
                                <Table.Cell collapsing textAlign='center' content={unitCount}/>
                                <Table.Cell collapsing textAlign='center' content={colleagueCount}/>
                                <Table.Cell collapsing textAlign='center' content={cvColleagueCount}/>
                                <Table.Cell collapsing textAlign='center' content={`${cvColleagueRate}%`}/>
                                <Table.Cell collapsing content='-'/>
                            </Table.Row>
                        );
                    })() : null}
                    {sortedIdols.map(idol => {
                        const {units, tagURL, dicURL, index} = idol;
                        return (
                            <Table.Row key={idol.characterID}>
                                <Table.Cell collapsing textAlign='center' content={index + 1}/>
                                <Table.Cell collapsing textAlign='center' content={idol.idolType}/>
                                <Table.Cell selectable content={<Link to={`/idol/${idol.characterName}`}>{idol.characterName} ({idol.yomi})</Link>}/>
                                <Table.Cell collapsing textAlign='center' content={idol.age}/>
                                <Table.Cell collapsing textAlign='center' content={idol.height}/>
                                <Table.Cell collapsing textAlign='center' content={idol.unitCount}/>
                                <Table.Cell collapsing textAlign='center' content={idol.colleagueCount}/>
                                <Table.Cell collapsing textAlign='center' content={idol.cvColleagueCount}/>
                                <Table.Cell collapsing textAlign='center' content={`${idol.cvColleagueRate}%`}/>
                                <Table.Cell collapsing content={<div><a href={tagURL}>tag</a> / <a href={dicURL}>dic</a></div>}/>
                            </Table.Row>
                        );
                    })}
                </Table.Body>
            </Table>
        )
    }
}

IdolDisplayTable.propTypes = {
    idols: PropTypes.arrayOf(PropTypes.shape({
        characterID: PropTypes.number.isRequired,
        characterName: PropTypes.string.isRequired,
        idolType: PropTypes.string.isRequired,
        yomi: PropTypes.string.isRequired,
        age: PropTypes.number,
        height: PropTypes.number.isRequired,
        units: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
        colleagues: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
        cvColleagueCount: PropTypes.number.isRequired,
    })).isRequired,
    showAverage: PropTypes.bool,
};

IdolDisplayTable.defaultProps = {
    showAverage: false,
};

export default IdolDisplayTable;
