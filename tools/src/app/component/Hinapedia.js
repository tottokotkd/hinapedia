/**
 * Created by tottokotkd on 31/01/2017.
 */

import React, {Component, PropTypes} from 'react'
import {routerShape, Link} from 'react-router'
import {intlShape} from 'react-intl';
import {Header, Menu, Button, Container, Grid, Message, Label, Icon, List, Divider, Segment, Table, Dimmer, Loader} from 'semantic-ui-react';

import {MENU_ITEM} from '../constant/Mode'
import {headerMessages} from '../intl/messages'

class Hinapedia extends Component {

    componentDidMount() {
        this.props.loadAllIdols();
        this.props.loadAllUnits();
    }

    render() {
        const {selectedMenuItem, selectMenuItem, router, loading} = this.props;
        const {formatMessage} = this.props.intl;
        const {pathname} = this.props.location;

        return (
            <Grid style={{paddingTop: '1em'}}>

                <Dimmer inverted active={loading}>
                    <Loader>Loading</Loader>
                </Dimmer>

                <Grid.Row>
                    <Grid.Column>
                        <Container>
                            <Header as='h2' icon='newspaper'
                                    content={formatMessage(headerMessages.title)}
                                    subheader={formatMessage(headerMessages.subtitle)} />
                        </Container>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row style={{paddingTop: '0'}}>
                    <Grid.Column>
                        <Menu pointing secondary>
                            <Container>
                                {
                                    [
                                        {key: headerMessages.top, path: '/', item: MENU_ITEM.TOP},
                                        {key: headerMessages.idol, path: '/idol', item: MENU_ITEM.IDOL},
                                        {key: headerMessages.unit, path: '/unit', item: MENU_ITEM.UNIT},
                                        {key: headerMessages.dev, path: '/dev', item: MENU_ITEM.DEV},
                                    ].map(param => {
                                        return <Menu.Item
                                            key={param.key.id}
                                            name={param.key.id}
                                            content={<Link to={param.path} style={{color: 'black'}}>{formatMessage(param.key)}</Link>}
                                            active={param.item == selectedMenuItem}
                                        />
                                    })
                                }
                                <Menu.Item
                                    key={headerMessages.blog.id}
                                    name={headerMessages.blog.id}
                                    content={<a target='_blank' href="/hinapedia/info" style={{color: 'black'}}>{formatMessage(headerMessages.blog)}</a>}
                                />
                            </Container>
                        </Menu>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column>
                        <Container>
                            {this.props.children}
                        </Container>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row color="grey">
                    <Grid.Column>
                        <Container text>
                            <Grid>
                                <Grid.Row columns={2}>
                                    <Grid.Column>
                                        © 2017 tottokotkd
                                    </Grid.Column>
                                    <Grid.Column textAlign="right">
                                        <Button icon='twitter' circular size='mini' as='a' href="https://twitter.com/tottokotkd" />
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Container>
                    </Grid.Column>
                </Grid.Row>

            </Grid>
        );
    }
}

Hinapedia.propTypes = {
    loading: PropTypes.bool.isRequired,
    selectedMenuItem: PropTypes.symbol.isRequired,
    selectMenuItem: PropTypes.func.isRequired,
    loadAllIdols: PropTypes.func.isRequired,
    loadAllUnits: PropTypes.func.isRequired,

    // react-intl
    intl: intlShape,

    // react-router
    children: PropTypes.node,
    router: routerShape.isRequired
};

export default Hinapedia;
