/**
 * Created by tottokotkd on 30/01/2017.
 */

import React from 'react'
const {Component, PropTypes} = React;
import {intlShape} from 'react-intl';
import { Header, Table, Input, Button, Form, Grid, Dropdown, Container, Message, Divider, Checkbox, TextArea} from 'semantic-ui-react'
import last from 'lodash/last'
import findIndex from 'lodash/findIndex'
import sortBy from 'lodash/sortBy'
import defaultTo from 'lodash/defaultTo'
import Clipboard from 'clipboard'

import {IDOL_SEARCH_MODE} from '../../constant/Mode'
import {headerMessages} from '../../intl/messages'

class DevelopIdolList extends Component {

    state = {
        unitValue: '',
        onlyEmptyUnitMode: true,
        copySuccess: null,
    };

    componentDidMount() {
        const clip = new Clipboard('#copyButton');

        clip.on('success', e => {
            console.log('event', e);
            console.log('state', this.state);
            this.setState({copySuccess: true});
            e.clearSelection();
        });

        clip.on('error', e => {
            this.setState({copySuccess: false});
        });
    }

    render() {
        const {idols, setMode, mode, query, setQuery} = this.props;
        const {formatMessage} = this.props.intl;
        const shawOnlyMatched = !!query;
        const toShow = unit => !this.state.onlyEmptyUnitMode || unit.idols.length == 0;
        const units = sortBy(this.props.units.filter(toShow), 'unitID');
        return (
            <div>
                <div>消すのが面倒くさいので出しっぱなしの開発ツール (手作業)</div>
                <Divider clearing style={{margin: '2em'}} />
                <Grid container verticalAlign='middle' textAlign='center'>
                    <Grid.Row>
                        <Grid.Column width="12">
                            <Header as='h3' content='ユニットデータ生成'/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width="10">
                            <Dropdown placeholder='unit' fluid selection search
                                      options={units.map(unit=> {return {text: unit.unitName, value: unit.unitID}})}
                                      value={this.state.unitValue}
                                      onChange={(e, {value}) => this.setState({unitValue: value})} />
                        </Grid.Column>
                        <Grid.Column width="3">
                            <Checkbox label='empty only' checked={this.state.onlyEmptyUnitMode} onChange={() => this.setState({unitValue: '', onlyEmptyUnitMode: !this.state.onlyEmptyUnitMode})} />
                        </Grid.Column>
                        <Grid.Column width="3" >
                            <Button.Group size="mini" fluid>
                                <Button onClick={() => {
                                    setQuery('');
                                    this.setState({copySuccess: null});
                                    if (units.length && this.state.unitValue === '') {
                                        this.setState({unitValue: last(units).unitID});
                                    } else {
                                        const nextIndex = findIndex(units, ['unitID', this.state.unitValue]) - 1;
                                        this.setState({unitValue: nextIndex < 0 ? '' : units[nextIndex].unitID});
                                    }
                                }}>prev</Button>
                                <Button onClick={() => {
                                    setQuery('');
                                    this.setState({copySuccess: null});
                                    const nextIndex = findIndex(units, ['unitID', this.state.unitValue]) + 1;
                                    this.setState({unitValue: nextIndex >= units.length ? '' : units[nextIndex].unitID});
                                }}>next</Button>
                            </Button.Group>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width="12">
                            <Input placeholder="アイドル一覧ページと同等の検索クエリに対応" value={defaultTo(query, '')} onChange={(event, data) => setQuery(data.value)} fluid />
                        </Grid.Column>
                        <Grid.Column width="4" >
                            <Button.Group size="mini" fluid>
                                <Button positive={mode == IDOL_SEARCH_MODE.AND} onClick={() => setMode(IDOL_SEARCH_MODE.AND)}>AND</Button>
                                <Button positive={mode == IDOL_SEARCH_MODE.OR} onClick={() => setMode(IDOL_SEARCH_MODE.OR)}>OR</Button>
                            </Button.Group>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Table celled padded sortable >
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell>ID</Table.HeaderCell>
                                        <Table.HeaderCell>Type</Table.HeaderCell>
                                        <Table.HeaderCell>Name</Table.HeaderCell>
                                        <Table.HeaderCell>Pixiv</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    {idols.map(idol => {
                                        if (shawOnlyMatched && !idol.isMatch) {
                                            return null;
                                        }
                                        const dicURL = `http://dic.pixiv.net/a/${idol.characterName}`;
                                        return (
                                            <Table.Row key={idol.characterName}>
                                                <Table.Cell singleLine>{idol.characterID}</Table.Cell>
                                                <Table.Cell singleLine>{idol.idolType}</Table.Cell>
                                                <Table.Cell singleLine>{idol.characterName} ({idol.yomi})</Table.Cell>
                                                <Table.Cell singleLine><a href={dicURL} target="_blank">dic</a></Table.Cell>
                                            </Table.Row>
                                        );
                                    })}
                                </Table.Body>
                            </Table>
                        </Grid.Column>
                    </Grid.Row>
                    { (this.state.unitValue && query) ? (() => {
                            const target = idols.filter(idol => idol.isMatch);
                            if (target.length == 0) {
                                return null;
                            }

                            const value = target
                                .map(idol => `${this.state.unitValue},${idol.characterID},"[,)"`)
                                .join("\n");

                            const content = this.state.copySuccess === null ? 'copy' : (this.state.copySuccess ? 'copied!' : 'press Ctrl + c' );

                            return (
                                <Grid.Row>
                                    <Grid.Column width="12">
                                        <Form>
                                            <TextArea id="unitsTextArea" value={value} autoHeight/>
                                        </Form>
                                    </Grid.Column>
                                    <Grid.Column width="4">
                                        <Button id="copyButton" content={content} data-clipboard-target="#unitsTextArea"/>
                                    </Grid.Column>
                                </Grid.Row>
                            );
                        })() : null}
                </Grid>
            </div>
        )
    }
}

DevelopIdolList.propTypes = {
    query: PropTypes.string,
    mode: PropTypes.symbol,
    idols: PropTypes.arrayOf(PropTypes.shape({
        characterID: PropTypes.number.isRequired,
        characterName: PropTypes.string.isRequired,
        idolType: PropTypes.string.isRequired,
        yomi: PropTypes.string.isRequired
    })).isRequired,
    units: PropTypes.arrayOf(PropTypes.shape({
        unitID: PropTypes.number.isRequired,
        unitName: PropTypes.string.isRequired,
        idols: PropTypes.arrayOf(PropTypes.shape({
            characterName: PropTypes.string.isRequired,
            yomi: PropTypes.string.isRequired,
            idolType: PropTypes.string.isRequired,
        })).isRequired,
    })).isRequired,
    setQuery: PropTypes.func.isRequired,
    setMode: PropTypes.func.isRequired,
    reload: PropTypes.func.isRequired,

    // react-intl
    intl: intlShape
};

DevelopIdolList.defaultProps = {
    list: [],
    changeQuery: () => {},
    reload: () => {}
};

export default DevelopIdolList;
