/**
 * Created by tottokotkd on 2016/12/27.
 */

import React from 'react'
const {Component, PropTypes} = React;
import {Router, Route, IndexRoute, IndexRedirect, hashHistory, Redirect, routerShape, locationShape} from 'react-router'
import {IntlProvider, addLocaleData} from 'react-intl'
import ja from 'react-intl/locale-data/ja'
import keys from 'lodash/keys'
import flow from 'lodash/flow'

addLocaleData([...ja]);

import {MENU_ITEM} from '../constant/Mode'
import HinapediaContainer from '../container/Hinapedia'
import Top from './top'
import AllIdolList from '../container/idol/AllIdolList'
import Idol from '../container/idol/Idol'
import Unit from '../container/unit/Unit'
import AllUnitList from '../container/unit/AllUnitList'
import DevTools from '../container/dev'

hashHistory.listen(e => {
    if (window.ga && window.location && e.action === 'POP') {
        ga('set', 'page', location.pathname + location.search + location.hash);
        window.ga('send', 'pageview');
    }
});

class App extends Component {
    render() {
        const {locale, messages, selectMenuItem, searchIdol, searchUnit} = this.props;

        const idolListHandler = (nextState, replace, callback) => {

            const query = nextState.location.query;
            if (keys(query).length == 0) {
                callback();
                return;
            }

            const {filter, sort, version} = query;

            const v1 = (filter, sort) => {
                const {setIdolListSearchQuery, setIdolListSortQuery} = this.props;
                if (filter) {
                    flow(decodeURIComponent, setIdolListSearchQuery)(filter);
                }
                if (sort) {
                    flow(decodeURIComponent, setIdolListSortQuery)(sort);
                }
                callback();
            };

            switch (version) {
                case '1':
                default:
                    v1(filter, sort);
                    break;
            }
        };

        return (
            <IntlProvider locale={locale} messages={messages}>
                <Router history={hashHistory}>
                    <Route path="/" component={HinapediaContainer}>
                        <IndexRoute onEnter={() => selectMenuItem(MENU_ITEM.TOP)} component={Top}/>
                        <Route path="idol" onEnter={() => selectMenuItem(MENU_ITEM.IDOL)}>
                            <IndexRoute onEnter={idolListHandler} component={AllIdolList}/>
                            <Route path=":name" component={Idol} onEnter={({params}, replace) => searchIdol(params.name)} />
                        </Route>
                        <Route path="unit" onEnter={() => selectMenuItem(MENU_ITEM.UNIT)}>
                            <IndexRoute component={AllUnitList}/>
                            <Route path=":name"  component={Unit}  onEnter={({params}, replace) => searchUnit(params.name)} />
                            <Route path="*" onEnter={({params}, replace) => replace(`/unit/${params.splat.replace('/', '_')}`)} component={Unit}/>
                        </Route>
                        <Route path="dev" onEnter={() => selectMenuItem(MENU_ITEM.DEV)} component={DevTools.DevelopIdolListContainer}/>
                        <Route path="*" onEnter={() => selectMenuItem(MENU_ITEM.NONE)} component={() => <p>not found</p>}/>
                    </Route>
                </Router>
            </IntlProvider>
        );
    }
}

App.propTypes= {
    locale: PropTypes.string,
    messages: PropTypes.any,
    selectMenuItem: PropTypes.func.isRequired,
    setIdolListSearchQuery: PropTypes.func.isRequired,
    setIdolListSortQuery: PropTypes.func.isRequired,
    searchIdol: PropTypes.func.isRequired,
    searchUnit: PropTypes.func.isRequired,
};

export default App;
