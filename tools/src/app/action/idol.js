/**
 * Created by tottokotkd on 29/01/2017.
 */

import round from 'lodash/round'
import defaultTo from 'lodash/defaultTo'

import * as Type from '../constant/ActionType'
import {IDOL_SEARCH_STATUS} from '../constant/Mode'

/*
    list
 */
export const setSearchMode = mode => {
    return {type: Type.SET_IDOL_LIST_SEARCH_MODE, mode}
};

export const setIdolListSearchQuery = query => {
    return {type: Type.SET_IDOL_LIST_SEARCH_QUERY, query}
};

export const setIdolListSortQuery = query => {
    return {type: Type.SET_IDOL_LIST_SORT_QUERY, query}
};

export const loadAllIdols = () => dispatch => {
    fetch('/hinapedia/data/idols')
        .then(response => response.json())
        .then(json => {
            const idols = defaultTo(json.idols, []);
            const data = idols.map(item => {
                const unitCount = item.units.length;
                const colleagueCount = item.colleagues.length;
                const cvColleagueCount = item.cv_colleague_count;
                const cvColleagueRate = round(cvColleagueCount / colleagueCount * 100, 2);
                return {
                    characterID: item.character_id,
                    characterName: item.character_name,
                    idolType: item.idol_type,
                    yomi: item.yomi,
                    age: item.age,
                    birthDate: {
                        month: item.birth_month,
                        day: item.birth_day,
                    },
                    height: item.height_cm,
                    weight: item.weight_kg,
                    bust: item.bust_cm,
                    waist: item.waist_cm,
                    hip: item.hip_cm,
                    bloodType: item.blood_type,
                    actor: item.actor_name,
                    units: item.units,
                    unitCount,
                    colleagues: item.colleagues,
                    colleagueCount,
                    cvColleagueCount,
                    cvColleagueRate,
                }
            });
            dispatch({type: Type.LOAD_IDOLS_DATA, data})
        });
};

/*
    individual
 */
export const searchIdol = name => {
    return dispatch => {
        dispatch({type: Type.SET_IDOL_SEARCH_STATUS, status: IDOL_SEARCH_STATUS.LOADING});
        dispatch({type: Type.SET_IDOL_SEARCH_QUERY, query: name});
        fetch('/hinapedia/data/idol/' + encodeURIComponent(name))
            .then(response => response.json())
            .then(json => {
                const {character_id, character_name, yomi, idol_type, age, birth_day, birth_month, actor_name} = json;
                const units = defaultTo(json.units, []).map(unit => {
                    const {unit_id, unit_name} = unit;
                    const idols = defaultTo(unit.idols, []).map(idol => {
                        const {character_id, character_name, yomi, idol_type, age, birth_day, birth_month, actor_name} = idol;
                        return {
                            characterID: character_id,
                            characterName: character_name,
                            idolType: idol_type,
                            birthDay: birth_day,
                            birthMonth: birth_month,
                            actorName: actor_name,
                            yomi,
                            age,
                        };
                    });
                    return {
                        unitID: unit_id,
                        unitName: unit_name,
                        idols,
                    }
                });
                const data ={
                    characterID: character_id,
                    characterName: character_name,
                    idolType: idol_type,
                    birthDay: birth_day,
                    birthMonth: birth_month,
                    actorName: actor_name,
                    yomi,
                    age,
                    units,
                };
                dispatch({type: Type.LOAD_IDOL_INDIVIDUAL_DATA, data: data});
                dispatch({type: Type.SET_IDOL_SEARCH_STATUS, status: IDOL_SEARCH_STATUS.FOUND});
            })
            .catch(error => {
                dispatch({type: Type.SET_IDOL_SEARCH_STATUS, status: IDOL_SEARCH_STATUS.NOT_FOUND});
            });
    }
};
