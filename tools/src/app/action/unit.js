/**
 * Created by tottokotkd on 29/01/2017.
 */

import defaultTo from 'lodash/defaultTo'

import * as Type from '../constant/ActionType'
import {UNIT_SEARCH_STATUS} from '../constant/Mode'


/*
 list
 */
export const setSearchMode = mode => {
    return {type: Type.SET_UNIT_LIST_SEARCH_MODE, mode}
};

export const setUnitListSearchQuery = query => {
    return {type: Type.SET_UNIT_LIST_SEARCH_QUERY, query}
};

export const setUnitListPage = page => {
    return {type: Type.SET_UNIT_LIST_PAGE, page}
};

export const loadAllUnits = () => dispatch => {
    fetch('/hinapedia/data/units')
        .then(response => response.json())
        .then(json => {
            const units = defaultTo(json.units, []);
            const data = units.map(unit => {
                const {unit_id, unit_name} = unit;
                const idols = defaultTo(unit.idols, []).map(idol => {
                    const {character_id, character_name, yomi, idol_type, actor_name} = idol;
                    return {
                        characterID: character_id,
                        characterName: character_name,
                        idolType: idol_type,
                        actorName: actor_name,
                        yomi,
                    };
                });
                return {
                    unitID: unit_id,
                    unitName: unit_name,
                    idols,
                };
            });
            dispatch({type: Type.LOAD_UNITS_DATA, data})
        });
};

/*
 individual
 */
export const searchUnit = name => dispatch => {
    dispatch({type: Type.SET_UNIT_SEARCH_STATUS, status: UNIT_SEARCH_STATUS.LOADING});
    dispatch({type: Type.SET_UNIT_SEARCH_QUERY, query: name});
    fetch('/hinapedia/data/unit/' + encodeURIComponent(name))
        .then(response => response.json())
        .then(json => {
            const {unit_id, unit_name} = json;
            const idols = defaultTo(json.idols, []).map(idol => {
                const {character_id, character_name} = idol;
                return {
                    characterID: character_id,
                    characterName: character_name,
                }
            });
            const data = {
                unitID: unit_id,
                unitName: unit_name,
                idols,
            };
            dispatch({type: Type.LOAD_UNIT_INDIVIDUAL_DATA, data});
            dispatch({type: Type.SET_UNIT_SEARCH_STATUS, status: UNIT_SEARCH_STATUS.FOUND});
        })
        .catch(error => {
            dispatch({type: Type.SET_UNIT_SEARCH_STATUS, status: UNIT_SEARCH_STATUS.NOT_FOUND});
        });
};
