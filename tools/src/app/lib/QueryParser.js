/**
 * Created by tottokotkd on 16/02/2017.
 */

import flow from 'lodash/flow'
import toInt from 'lodash/toNumber'
import trim from 'lodash/trim'
import {parse} from 'lucene-query-parser'

class QueryParser {

    static rangeValueChecker = (query, {
        splitter = '..',
        formatter = flow(trim, s => s == '' ? null : toInt(s)),
        equal = (left, right) => left == right,
        compare = (left, right) => left != null && right != null && left <= right,
    } = {}) => target => {

        const {term_max, term_min, inclusive} = query;
        if (term_max != null && term_min != null) {
            return inclusive
                ? compare(term_min, target) && compare(target, term_max)
                : !equal(term_min, target) && compare(term_min, target) && compare(target, term_max) && !equal(term_max, target);
        }

        const {term} = query;
        const array = term.split(splitter);
        if (array.length != 2) {
            return equal(term, target);
        }

        const start = formatter(array[0]);
        const end = formatter(array[1]);

        if (start == null && end == null) {
            return true
        }

        if (start == null) {
            return compare(target, end);
        }

        if (end == null) {
            return compare(start, target);
        }

        return compare(start, target) && compare(target, end);
    };

    handleFields = query => target => {};

    constructor(handler) {
        this.handleFields = handler;
    }

    handleOperators = operator => (a, b) => target => {
        switch (operator) {
            case 'AND':
                return a(target) && b(target);

            case 'OR':
                return a(target) || b(target);

            case 'NOT': {
                return a(target) && !b(target);
            }


            default:
                return a(target) && b(target);
        }
    };

    handlePrefixes = prefix => boolValue => {
        switch (prefix) {
            case '-':
                return !boolValue;

            default:
                return boolValue;
        }
    };

    match = query =>  flow(this.handleFields(query), this.handlePrefixes(query.prefix));

    read = obj => {
        if (obj.right) {
            const {left, right, operator} = obj;
            const op = this.handleOperators(operator);
            return op(this.read(left), this.read(right));
        } else if (obj.left) {
            const {left} = obj;
            return this.match(left);
        } else {
            return this.match(obj);
        }
    };

    parse(query) {
        return flow(parse, this.read)(query);
    };

    parseSafely(query) {
        try {
            return flow(parse, this.read)(query);
        } catch (err) {
            return () => false;
        }
    };

}

export default QueryParser;
