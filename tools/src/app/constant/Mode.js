/**
 * Created by tottokotkd on 2017/01/11.
 */

// menu item
export const MENU_ITEM = {
    TOP: Symbol('menu item: top'),
    IDOL: Symbol('menu item: idol'),
    UNIT: Symbol('menu item: unit'),
    DEV: Symbol('menu item: developer tool'),
    NONE: Symbol('menu item: none'),
};

// idol search mode
export const IDOL_SEARCH_MODE = {
    AND: Symbol('idol search mode = AND'),
    OR: Symbol('idol search mode = OR')
};

// idol search status
export const IDOL_SEARCH_STATUS = {
    EMPTY: Symbol('idol search result = empty'),
    LOADING: Symbol('idol search result = loading'),
    NOT_FOUND: Symbol('idol search result = not found'),
    FOUND: Symbol('idol search result = found'),
};

// unit search mode
export const UNIT_SEARCH_MODE = {
    AND: Symbol('unit search mode = AND'),
    OR: Symbol('unit search mode = OR')
};

// unit search status
export const UNIT_SEARCH_STATUS = {
    EMPTY: Symbol('unit search result = empty'),
    LOADING: Symbol('unit search result = loading'),
    NOT_FOUND: Symbol('unit search result = not found'),
    FOUND: Symbol('unit search result = found'),
};
