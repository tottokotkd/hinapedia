/**
 * Created by tottokotkd on 29/01/2017.
 */

import {combineReducers} from 'redux'
import list from './list'
import individual from './individual'

export default combineReducers({list, individual});
