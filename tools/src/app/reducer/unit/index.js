/**
 * Created by tottokotkd on 02/02/2017.
 */

import {combineReducers} from 'redux'
import list from './list'
import individual from './individual'

export default combineReducers({list, individual});
