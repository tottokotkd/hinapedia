/**
 * Created by tottokotkd on 02/02/2017.
 */

import assign from 'lodash/assign'

import * as Type from '../../constant/ActionType'
import {UNIT_SEARCH_STATUS} from '../../constant/Mode'

const defaultState = () => {
    return {
        status: UNIT_SEARCH_STATUS.EMPTY,
        query: null,
        unitData: null,
    }
};

export default (state = defaultState(), action) => {
    const update = newValue => assign({}, state, newValue);

    switch (action.type) {
        case Type.SET_UNIT_SEARCH_QUERY: {
            return update({query: action.query});
        }

        case Type.SET_UNIT_SEARCH_STATUS: {
            return update({status: action.status});
        }

        case Type.LOAD_UNIT_INDIVIDUAL_DATA: {
            return update({unitData: action.data});
        }
    }

    return state;
};
