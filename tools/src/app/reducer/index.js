/**
 * Created by tottokotkd on 03/01/2017.
 */

import {combineReducers} from 'redux'
import app from './app'
import idol from './idol'
import unit from './unit'
import user from './user'

export const reducer = combineReducers({app, user, unit, idol});
