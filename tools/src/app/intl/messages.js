/**
 * Created by tottokotkd on 25/01/2017.
 */

import {defineMessages} from 'react-intl'

const directory = 'messages';

const headerGroup = 'headerGroup';
export const headerMessages = defineMessages({
    title: {
        id: `${directory}.${headerGroup}.title`,
        defaultMessage: "hinapedia",
        description: "site title"
    },
    subtitle: {
        id: `${directory}.${headerGroup}.subtitle`,
        defaultMessage: "シンデレラガールズDB",
        description: "site subtitle"
    },
    top: {
        id: `${directory}.${headerGroup}.top`,
        defaultMessage: "Top",
        description: "menu item for 'top' page"
    },
    idol: {
        id: `${directory}.${headerGroup}.idol`,
        defaultMessage: "Idol",
        description: "menu item for 'idol' page"
    },
    unit: {
        id: `${directory}.${headerGroup}.unit`,
        defaultMessage: "Unit",
        description: "menu item for 'unit' page"
    },
    dev: {
        id: `${directory}.${headerGroup}.devTools`,
        defaultMessage: "Dev tools",
        description: "menu item for 'dev tools' page"
    },
    blog: {
        id: `${directory}.${headerGroup}.infoBlog`,
        defaultMessage: "Info",
        description: "menu item for 'info' page"
    }
});
