#!/usr/bin/env bash

npm run build --prefix ./tools

docker-compose -f ./sql/docker-compose.yml up -d
docker-compose -f ./sql/docker-compose.yml exec db bash /hinapedia/sql/deploy.sh /hinapedia hinaDB localhost
mv dist/* ./tools/build/hinapedia/data/
