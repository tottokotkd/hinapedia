SET SEARCH_PATH TO hinapedia;

\set idols_output_path :path'/idols'
COPY (
WITH src AS (
  SELECT jsonb_agg(idol_summaries_json_view) as idols FROM idol_summaries_json_view
) SELECT row_to_json(src) FROM src
) TO :'idols_output_path';

\set units_output_path :path'/units'
COPY (
  WITH src AS (
    SELECT jsonb_agg(units_json_view) as units FROM units_json_view
  ) SELECT row_to_json(src) FROM src
) TO :'units_output_path';

DROP FUNCTION IF EXISTS output_idol(TEXT);
CREATE OR REPLACE FUNCTION output_idol(TEXT) RETURNS INT AS $$
declare
  count INTEGER;
  ref RECORD;
  path TEXT;
begin
  count := 0;
  FOR ref IN SELECT * FROM hinapedia.idols_json_view LOOP
    path := ' ''' || $1 || replace(ref.character_name, '''', '''''') || ''' ';
    EXECUTE 'COPY (SELECT json_strip_nulls(row_to_json(idols_json_view)) FROM idols_json_view WHERE character_id = ' || ref.character_id || ') TO ' || path;
    count := count + 1;
  END LOOP;

  return count;
end;
$$ language 'plpgsql';

\set idols_output_path :path'/idol/'
select output_idol(:'idols_output_path');

DROP FUNCTION IF EXISTS output_unit(TEXT);
CREATE OR REPLACE FUNCTION output_unit(TEXT) RETURNS INT AS $$
declare
  count INTEGER;
  ref RECORD;
  path TEXT;
begin
  count := 0;
  FOR ref IN SELECT * FROM hinapedia.units_json_view LOOP
    path := replace(ref.unit_name, '/', '_');
    path := replace(path, '''', '''''');
    path := ' ''' || $1 || path || ''' ';
    EXECUTE 'COPY (SELECT json_strip_nulls(row_to_json(units_json_view)) FROM units_json_view WHERE unit_id = ' || ref.unit_id || ') TO ' || path;
    count := count + 1;
  END LOOP;

  return count;
end;
$$ language 'plpgsql';

\set units_output_path :path'/unit/'
select output_unit(:'units_output_path');
