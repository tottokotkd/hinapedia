#!/usr/bin/env bash

case $# in
0)
    echo 'target path required.';
    exit -1;
   ;;
1)
    path=$1
    database=postgres;
    hostname=localhost;
    ;;
2)
    path=$1
    database=$2;
    hostname=localhost;
    ;;
3)
    path=$1
    database=$2;
    hostname=$3;
    ;;
*)
    echo 'invalid arguments.';
    exit -1;
    ;;
esac

psql -f ${path}/sql/01_tables.sql -U postgres -d ${database} -h ${hostname}
psql -f ${path}/sql/02_import.sql -U postgres -d ${database} -h ${hostname} -v path=${path}/data
psql -f ${path}/sql/03_view.sql -U postgres -d ${database} -h ${hostname}
rm -rf ${path}/dist/data
mkdir -p ${path}/dist/data/idol
mkdir -p ${path}/dist/data/unit
chmod a+w ${path}/dist/data
chmod a+w ${path}/dist/data/idol
chmod a+w ${path}/dist/data/unit
psql -f ${path}/sql/output.sql -U postgres -d ${database} -h ${hostname} -v path=${path}/dist/data
