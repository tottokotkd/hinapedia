SET SEARCH_PATH TO hinapedia;

CREATE OR REPLACE VIEW colleague_cv_info AS
  WITH colleagues_info AS (
    SELECT DISTINCT
      characters.id AS character_id,
      colleagues_of_unit.character_id AS colleague_id,
      actor_of_character.actor_id IS NOT NULL AS has_actor
    FROM characters
      LEFT JOIN characters_of_unit ON characters.id = characters_of_unit.character_id
      LEFT JOIN characters_of_unit AS colleagues_of_unit ON characters_of_unit.unit_id = colleagues_of_unit.unit_id AND characters_of_unit.character_id <> colleagues_of_unit.character_id
      LEFT JOIN actor_of_character ON colleagues_of_unit.character_id = actor_of_character.character_id
  ), colleague_voice_info AS (
    SELECT
      character_id,
      array_agg(colleagues.name) AS colleagues,
      sum(CASE WHEN has_actor THEN 1 ELSE 0 END) as cv_colleague_count,
      count(*) as total_colleague_count
    FROM colleagues_info
      JOIN characters AS colleagues ON colleagues_info.colleague_id = colleagues.id
    GROUP BY character_id
  )
  SELECT
    characters.id AS character_id,
    characters.name AS character_name,
    array_length(colleagues, 1) AS colleague_count,
    cv_colleague_count AS cv_count,
    trunc(cv_colleague_count::NUMERIC / total_colleague_count * 100, 2) AS cv_rate,
    colleagues,
    actor_of_character.actor_id IS NOT NULL AS has_cv
  FROM characters
    JOIN colleague_voice_info ON characters.id = colleague_voice_info.character_id
    LEFT JOIN actor_of_character ON characters.id = actor_of_character.character_id;

CREATE OR REPLACE VIEW idols_view AS
  SELECT
    characters.id as character_id,
    characters.name as character_name,
    idol_type,
    age,
    birth_month, birth_day,
    (height_of_character.metre * 100)::INT as height_cm,
    (weight_of_character.kilogramme)::INT as weight_kg,
    (bwh_measurement_of_character.bust_metre * 100)::INT as bust_cm,
    (bwh_measurement_of_character.waist_metre * 100)::INT as waist_cm,
    (bwh_measurement_of_character.hip_metre * 100)::INT as hip_cm,
    blood_type_of_character.blood_type,
    yomi,
    actors.name as actor_name,
    to_jsonb(colleague_cv_info.colleagues) AS colleagues,
    colleague_cv_info.colleague_count AS total_colleague_count,
    colleague_cv_info.cv_count AS cv_colleague_count
  FROM characters
    JOIN yomi_of_character ON characters.id = yomi_of_character.character_id
    JOIN idol_type_of_character ON characters.id = idol_type_of_character.character_id
    LEFT JOIN age_of_character ON characters.id = age_of_character.character_id
    LEFT JOIN birthday_of_character ON characters.id = birthday_of_character.character_id
    LEFT JOIN height_of_character ON characters.id = height_of_character.character_id
    LEFT JOIN weight_of_character ON characters.id = weight_of_character.character_id
    LEFT JOIN bwh_measurement_of_character ON characters.id = bwh_measurement_of_character.character_id
    LEFT JOIN blood_type_of_character ON characters.id = blood_type_of_character.character_id
    LEFT JOIN actor_of_character ON characters.id = actor_of_character.character_id
    LEFT JOIN actors ON actor_of_character.actor_id = actors.id
    LEFT JOIN colleague_cv_info ON characters.id = colleague_cv_info.character_id
  ORDER BY
    idol_type = 'Cu' DESC ,
    idol_type = 'Co' DESC ,
    idol_type = 'Pa' DESC ,
    yomi ASC;

CREATE OR REPLACE VIEW units_json_view AS
  WITH idol_list AS (
    SELECT unit_id, json_agg(idols_view) as idols
    FROM idols_view
      JOIN characters_of_unit ON idols_view.character_id = characters_of_unit.character_id
    GROUP BY unit_id
  ), unit_info AS (
    SELECT
      units.id as unit_id,
      units.name as unit_name,
      idol_list.idols
    FROM units
      LEFT JOIN idol_list ON units.id = idol_list.unit_id
  )
  SELECT *
  FROM unit_info;

CREATE OR REPLACE VIEW idols_json_view AS
  WITH unit_list AS (
    SELECT character_id, jsonb_agg(units_json_view) as units
    FROM units_json_view
      JOIN characters_of_unit ON units_json_view.unit_id = characters_of_unit.unit_id
    GROUP BY character_id
  ), idol_info AS (
    SELECT
      idols_view.*,
      unit_list.units
    FROM idols_view
      LEFT JOIN unit_list USING (character_id)
  )
  SELECT *
  FROM idol_info;

CREATE OR REPLACE VIEW idol_summaries_json_view AS
  WITH unit_list AS (
    SELECT character_id, jsonb_agg(units.name) as units
    FROM units
      JOIN characters_of_unit ON units.id = characters_of_unit.unit_id
    GROUP BY character_id
  ), idol_info AS (
    SELECT
      idols_view.*,
      unit_list.units
    FROM idols_view
      LEFT JOIN unit_list USING (character_id)
  )
  SELECT *
  FROM idol_info;
