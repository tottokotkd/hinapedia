SET SEARCH_PATH TO hinapedia;

-- ^\w*\.*\w*\.*(\w+)$
-- \\set $1_csv_path :path'/$1.csv'\nCOPY $1 from :'$1_csv_path' WITH CSV HEADER;\n

/*
    characters
*/

\set characters_csv_path :path'/characters.csv'
COPY characters from :'characters_csv_path' WITH CSV HEADER;

\set yomi_of_character_csv_path :path'/yomi_of_character.csv'
COPY yomi_of_character from :'yomi_of_character_csv_path' WITH CSV HEADER;

\set idol_types_csv_path :path'/idol_types.csv'
COPY idol_types from :'idol_types_csv_path' WITH CSV HEADER;

\set idol_type_of_character_csv_path :path'/idol_type_of_character.csv'
COPY idol_type_of_character from :'idol_type_of_character_csv_path' WITH CSV HEADER;

\set age_of_character_csv_path :path'/age_of_character.csv'
COPY age_of_character from :'age_of_character_csv_path' WITH CSV HEADER;

\set birthday_of_character_csv_path :path'/birthday_of_character.csv'
COPY birthday_of_character from :'birthday_of_character_csv_path' WITH CSV HEADER;

\set height_of_character_csv_path :path'/height_of_character.csv'
COPY height_of_character from :'height_of_character_csv_path' WITH CSV HEADER;

\set weight_of_character_csv_path :path'/weight_of_character.csv'
COPY weight_of_character from :'weight_of_character_csv_path' WITH CSV HEADER;

\set bwh_measurement_of_character_csv_path :path'/bwh_measurement_of_character.csv'
COPY bwh_measurement_of_character from :'bwh_measurement_of_character_csv_path' WITH CSV HEADER;

\set handedness_csv_path :path'/handedness.csv'
COPY handedness from :'handedness_csv_path' WITH CSV HEADER;

\set handedness_of_character_csv_path :path'/handedness_of_character.csv'
COPY handedness_of_character from :'handedness_of_character_csv_path' WITH CSV HEADER;

\set blood_types_csv_path :path'/blood_types.csv'
COPY blood_types from :'blood_types_csv_path' WITH CSV HEADER;

\set blood_type_of_character_csv_path :path'/blood_type_of_character.csv'
COPY blood_type_of_character from :'blood_type_of_character_csv_path' WITH CSV HEADER;

/*
    actors
*/

\set actors_csv_path :path'/actors.csv'
COPY actors from :'actors_csv_path' WITH CSV HEADER;

\set actor_of_character_csv_path :path'/actor_of_character.csv'
COPY actor_of_character from :'actor_of_character_csv_path' WITH CSV HEADER;

/*
    units
*/

\set units_csv_path :path'/units.csv'
COPY units from :'units_csv_path' WITH CSV HEADER;

\set characters_of_unit_csv_path :path'/characters_of_unit.csv'
COPY characters_of_unit from :'characters_of_unit_csv_path' WITH CSV HEADER;
